//
//  RenderBoxMain.h
//  RenderBox
//
//  Created by Mathieu Guindon on 2013-06-13.
//
//

#ifndef RenderBox_RenderBoxMain_h
#define RenderBox_RenderBoxMain_h

#include "PVRShell.h"
#include "OGLES2Tools.h"
#include "ICamera.h"
#include "OVR.h"
#include <vector>

/*!****************************************************************************
 Class implementing the PVRShell functions.
 ******************************************************************************/
class RenderBoxMain : public PVRShell,  public PVRTPFXEffectDelegate
{
    CPVRTgles2Ext m_sExtensions;

    SPVRTContext m_sContext;
	CPVRTPFXEffect    **m_ppPFXEffects;
	CPVRTMap<int, int> *m_pUniformMapping;
    
	// Print3D class used to display text
	CPVRTPrint3D	m_Print3D;
    
	// 3D Model
	CPVRTModelPOD	m_Scene;
    
	// OpenGL handles for shaders, textures and VBOs
	GLuint* m_puiVbo;
	GLuint* m_puiIndexVbo;
	GLuint* m_puiTextureIDs;
    GLuint* m_puiBumpTextureIDs;
    
	// The effect file handlers
	CPVRTPFXParser	*m_pEffectParser;
    int m_iRenderMeshEffectId;
    int m_iOculusLensEffectId;
    int m_iJustCopyEffectId;
    int m_iRenderToDepthEffectId;
    int m_iRenderShadowEffectId;
    int m_iLightScatteringEffectId;
    int m_iRenderWhiteEffectId;
    
	// Variables to handle the animation in a time-based manner
	unsigned long	m_ulTimePrev;
	float		m_fFrame;
    
    ICamera *m_Camera;
    
	GLint  m_i32OriginalFbo;
	GLuint m_uiFirstPassFBO;
    GLuint m_uiSecondPassFBO;
    GLuint m_uiThirdPassFBO;
    GLuint m_uiDownScaledFBO;
    GLuint m_uiDepthFBO;
    GLuint m_uiPrepareGIFBO;
    GLuint m_uiGaussianPassFBO;
	GLuint m_uiFirstPassTexture;
    GLuint m_uiFirstPassTexturePosition;
    GLuint m_uiFirstPassTextureNormal;
    GLuint m_uiFirstPassTextureColor;
    GLuint m_uiSecondPassTexture;
    GLuint m_uiThirdPassTexture;
    GLuint m_uiGaussianPassTexture;
    GLuint m_uiDepthTexture;
    GLuint m_uiGITexturePosition;
    GLuint m_uiGITextureNormal;
    GLuint m_uiGITextureColor;
    

	GLuint m_uiDepthBuffer;
    GLuint m_uiFirstPassDepthBuffer;
    GLuint m_uiColorBuffer;
    GLuint m_uiResolvedFBO;
    GLuint m_uiResolvedColorBuffer;
    int m_uiFirstPassFBOWidth;
    int m_uiFirstPassFBOHeight;
    int m_uiDownScaledFBOWidth;
    int m_uiDownScaledFBOHeight;
    int m_uiDepthFBOWidth;
    int m_uiDepthFBOHeight;
    PVRTMat4 m_BiasMatrix;

    // OVR
    OVR::Ptr<OVR::HMDDevice> m_pOVRHMD;
    OVR::HMDInfo m_OVRhmdInfo;
    OVR::Ptr<OVR::SensorDevice> m_pOVRSensor;
    OVR::SensorFusion m_OVRSensorFusion;
    OVR::Util::Render::Viewport m_leftEyeViewPort;
    OVR::Util::Render::Viewport m_rightEyeViewPort;
    OVR::Util::Render::DistortionConfig m_DistortionConfig;
    PVRTMat4 m_leftEyeProjectionMatrix;
    PVRTMat4 m_rightEyeProjectionMatrix;
    PVRTMat4 m_leftEyeAdjust;
    PVRTMat4 m_rightEyeAdjust;    
    float m_OVRRenderScale;

    bool m_bOculusSupport;
    
public:
	virtual bool InitApplication();
	virtual bool InitView();
	virtual bool ReleaseView();
	virtual bool QuitApplication();
	virtual bool RenderScene();
    void DrawAxisAlignedQuad(PVRTVec2 afLowerLeft, PVRTVec2 afUpperRight);
    void DrawAxisAlignedQuad(PVRTVec2 afLowerLeft, PVRTVec2 afLowerLeftUV, PVRTVec2 afUpperRight, PVRTVec2 afUpperRightUV);
    void DrawPoint(PVRTVec4 position);
    void DrawCircle(PVRTVec4 position, float radius);
    
	bool LoadTextures(CPVRTString* pErrorStr);
    bool LoadPFX(CPVRTString* pErrorStr);
	bool LoadVbos(CPVRTString* pErrorStr);
    bool CreateFBOS();
    bool InitOVR();
    
    void DrawMeshes(const PVRTMat4 &mView, const PVRTMat4 &mProjection, const PVRTMat4 & mShadowTextureTransform, int effectId, const std::vector<GLuint> *extraTextures);
	void DrawMesh(int i32MeshIndex);
    void HandleInput();
    void HandleOVROrientation();
    
    virtual EPVRTError PVRTPFXOnLoadTexture(const CPVRTStringHash& TextureName, GLuint& uiHandle, unsigned int& uiFlags);
};



#endif
