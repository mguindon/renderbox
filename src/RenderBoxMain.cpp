/******************************************************************************

 @File         RenderBoxMain.cpp

 @Title        Introducing the POD 3D file format

 @Version      

 @Copyright    Copyright (c) Imagination Technologies Limited.

 @Platform     Independent

 @Description  Shows how to load POD files and play the animation with basic
               lighting

******************************************************************************/

#include "RenderBoxMain.h"

#include <string.h>

#include "FPSCamera.h"
#include "OVRCamera.h"
#include "RenderBoxUtil.h"

#include <opengl/gl3.h>

using namespace OVR;
using namespace OVR::Util::Render;

/******************************************************************************
 Defines
******************************************************************************/
// Index to bind the attributes to vertex shaders
#define VERTEX_ARRAY	0
#define NORMAL_ARRAY	1
#define TEXCOORD_ARRAY	2

#define AXIS_ALIGNED_QUAD_VERTEX_ARRAY   0
#define AXIS_ALIGNED_QUAD_TEXCOORD_ARRAY 1

/******************************************************************************
 Consts
******************************************************************************/
// Camera constants. Used for making the projection matrix
const float g_fCameraNear = 0.3f;//2.0f;
const float g_fCameraFar  = 1000.0f;//5000.0f;

const float g_fDemoFrameRate = 1.0f / 30.0f;

// The camera to use from the pod file
const int g_ui32Camera = 0;

/******************************************************************************
 Content file names
******************************************************************************/

// Effect file
const char c_szPfxFile[]			= "effect.pfx";

// POD scene files
const char c_szSceneFile[]			= "sibenik.pod";

/*
 PVRTools includes a large number of built-in semantics and should cover most
 situations. However, it's possible to extend default semantics with user
 provided values. In this training course we are extending the default
 semantics by appending a custom 'scale' uniform which will be uses in
 the GLSL code.
 */
enum ECustomSemantic
{
    eUsLENSCENTER = ePVRTPFX_NumSemantics,
    eUsSCREENCENTER,
    eUsSCALE,
    eUsSCALEIN,
    eUsHMDWARPPARAM,
    eUsSHADOWTEXTURETRANSFORM,
    eUsTEXELSTEP,
    eUsLS_EXPOSURE,
    eUsLS_DECAY,
    eUsLS_DENSITY,
    eUsLS_WEIGHT,
    eUsLS_LIGHTPOSITIONONSCREEN
    // otherwise an error will be reported.
};

struct SPVRTPFXUniformSemantic c_sCustomSemantics[] =
{
	{ "LENSCENTER",	  eUsLENSCENTER	},
	{ "SCREENCENTER", eUsSCREENCENTER	},
    { "SCALE",	 	  eUsSCALE	},
    { "SCALEIN",	  eUsSCALEIN	},
    { "HMDWARPPARAM", eUsHMDWARPPARAM	},
    { "SHADOWTEXTURETRANSFORM", eUsSHADOWTEXTURETRANSFORM },
    { "TEXELSTEP", eUsTEXELSTEP},
    { "LS_EXPOSURE", eUsLS_EXPOSURE},
    { "LS_DECAY", eUsLS_DECAY},
    { "LS_DENSITY", eUsLS_DENSITY},
    { "LS_WEIGHT", eUsLS_WEIGHT},
    { "LS_LIGHTPOSITIONONSCREEN", eUsLS_LIGHTPOSITIONONSCREEN}
};

/*!****************************************************************************
 @Function		LoadTextures
 @Output		pErrorStr		A CPVRTString describing the error on failure
 @Return		bool			true if no error occured
 @Description	Loads the textures required for this training course
******************************************************************************/
bool RenderBoxMain::LoadTextures(CPVRTString* pErrorStr)
{
	/*
		Loads the textures.
		For a more detailed explanation, see Texturing and IntroducingPVRTools
	*/

	/*
		Initialises an array to lookup the textures
		for each material in the scene.
	*/
	m_puiTextureIDs = new GLuint[m_Scene.nNumMaterial];
    m_puiBumpTextureIDs = new GLuint[m_Scene.nNumMaterial];

	for(int i = 0; i < (int) m_Scene.nNumMaterial; ++i)
	{
		m_puiTextureIDs[i] = 0;
        m_puiBumpTextureIDs[i] = 0;
		SPODMaterial* pMaterial = &m_Scene.pMaterial[i];

		if(pMaterial->nIdxTexDiffuse != -1)
		{
			/*
				Using the tools function PVRTTextureLoadFromPVR load the textures required by the pod file.

				Note: This function only loads .pvr files. You can set the textures in 3D Studio Max to .pvr
				files using the PVRTexTool plug-in for max. Alternatively, the pod material properties can be
				modified in PVRShaman.
			*/

			CPVRTString sTextureName = m_Scene.pTexture[pMaterial->nIdxTexDiffuse].pszName;

			if(PVRTTextureLoadFromPVR(sTextureName.c_str(), &m_puiTextureIDs[i]) != PVR_SUCCESS)
			{
				*pErrorStr = "ERROR: Failed to load " + sTextureName + ".";

				// Check to see if we're trying to load .pvr or not
				CPVRTString sFileExtension = PVRTStringGetFileExtension(sTextureName);

				if(sFileExtension.toLower() != "pvr")
					*pErrorStr += "Note: IntroducingPOD can only load pvr files.";

				return false;
			}
            
            glBindTexture(GL_TEXTURE_2D, m_puiTextureIDs[i]);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
        
		if(pMaterial->nIdxTexBump != -1)
		{
			CPVRTString sTextureName = m_Scene.pTexture[pMaterial->nIdxTexBump].pszName;
            
			if(PVRTTextureLoadFromPVR(sTextureName.c_str(), &m_puiBumpTextureIDs[i]) != PVR_SUCCESS)
			{
				*pErrorStr = "ERROR: Failed to load " + sTextureName + ".";
                
				// Check to see if we're trying to load .pvr or not
				CPVRTString sFileExtension = PVRTStringGetFileExtension(sTextureName);
                
				if(sFileExtension.toLower() != "pvr")
					*pErrorStr += "Note: IntroducingPOD can only load pvr files.";
                
				return false;
			}
            
            glBindTexture(GL_TEXTURE_2D, m_puiBumpTextureIDs[i]);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		}
        
	}

	return true;
}

bool RenderBoxMain::LoadPFX(CPVRTString* pErrorStr)
{
    
	/*
     Load the effect file
     */ 
	CPVRTString	error;

    
	// Parse the file
	m_pEffectParser = new CPVRTPFXParser;
	if(m_pEffectParser->ParseFromFile(c_szPfxFile, &error) != PVR_SUCCESS)
	{
		PVRShellSet(prefExitMessage, error.c_str());
		return false;
	}

	// Setup all effects in the PFX file so we initialize the shaders and
	// store uniforms and attributes locations.
	unsigned int uiNumEffects = m_pEffectParser->GetNumberEffects();
	m_ppPFXEffects = new CPVRTPFXEffect*[uiNumEffects];
	m_pUniformMapping = new CPVRTMap<int,int>[uiNumEffects];
    
	for (unsigned int i=0; i < uiNumEffects; i++)
		m_ppPFXEffects[i] = new CPVRTPFXEffect(m_sContext);
    
	// Load one by one the effects. This will also compile the shaders.
	for (unsigned int i=0; i < uiNumEffects; i++)
	{
		if(m_ppPFXEffects[i]->RegisterUniformSemantic(c_sCustomSemantics, sizeof(c_sCustomSemantics) / sizeof(c_sCustomSemantics[0]), &error))
		{
			*pErrorStr = CPVRTString("Failed to set custom semantics:\n\n") + error;
			return false;
		}
        
		unsigned int nUnknownUniformCount = 0;
		if(m_ppPFXEffects[i]->Load(*m_pEffectParser, m_pEffectParser->GetEffect(i).Name.c_str(), NULL, NULL, nUnknownUniformCount, &error)  != PVR_SUCCESS)
		{
			*pErrorStr = CPVRTString("Failed to load effect ") + m_pEffectParser->GetEffect(i).Name.String() + CPVRTString(":\n\n") + error;
			return false;
		}
        
		// .. upps, some uniforms are not in our table. Better to quit because something is not quite right.
		if(nUnknownUniformCount)
		{
			*pErrorStr = CPVRTString("Unknown uniforms found in effect: ") + m_pEffectParser->GetEffect(i).Name.String();
			return false;
		}
        
		// Create the mapping so we can reference the uniforms more easily
		m_ppPFXEffects[i]->Activate();
		const CPVRTArray<SPVRTPFXUniform>& Uniforms = m_ppPFXEffects[i]->GetUniformArray();
		for(unsigned int j = 0; j < Uniforms.GetSize(); ++j)
		{
			m_pUniformMapping[i][Uniforms[j].nSemantic] = Uniforms[j].nLocation;
			if (Uniforms[j].nSemantic == ePVRTPFX_UsTEXTURE)
				glUniform1i(Uniforms[j].nLocation, Uniforms[j].nIdx);
		}
	}
    
    m_iRenderMeshEffectId = m_pEffectParser->FindEffectByName(CPVRTStringHash("RenderMesh"));
    m_iOculusLensEffectId = m_pEffectParser->FindEffectByName(CPVRTStringHash("OculusLens"));
    m_iJustCopyEffectId = m_pEffectParser->FindEffectByName(CPVRTStringHash("JustCopy"));
    m_iRenderToDepthEffectId = m_pEffectParser->FindEffectByName(CPVRTStringHash("RenderToDepth"));
    m_iRenderShadowEffectId = m_pEffectParser->FindEffectByName(CPVRTStringHash("RenderShadow"));
    m_iLightScatteringEffectId = m_pEffectParser->FindEffectByName(CPVRTStringHash("LightScattering"));
    m_iRenderWhiteEffectId = m_pEffectParser->FindEffectByName(CPVRTStringHash("RenderWhite"));
    
//	// --- Load an effect from the file
//	m_pEffect = new CPVRTPFXEffect();
//    
//	// Register a custom uniform
//	if(m_pEffect->RegisterUniformSemantic(c_sCustomSemantics, sizeof(c_sCustomSemantics) / sizeof(c_sCustomSemantics[0]), &error) != PVR_SUCCESS)
//	{
//		PVRShellSet(prefExitMessage, error.c_str());
//		return false;
//	}
//    
//	/*
//     Load the effect.
//     We pass 'this' as an argument as we wish to receive callbacks as the PFX is loaded.
//     This is optional and supplying NULL implies that the developer will take care
//     of all texture loading and binding to to the Effect instead.
//     */
//	if(m_pEffect->Load(*m_pEffectParser, "Effect", c_szPfxFile, NULL, uiUnknownUniforms, &error) != PVR_SUCCESS)
//	{
//		PVRShellSet(prefExitMessage, error.c_str());
//		return false;
//	}
//
//    m_pOculusLensEffect = new CPVRTPFXEffect();
//    
//	if(m_pOculusLensEffect->Load(*m_pEffectParser, "OculusLens", c_szPfxFile, NULL, uiUnknownUniforms, &error) != PVR_SUCCESS)
//	{
//		PVRShellSet(prefExitMessage, error.c_str());
//		return false;
//	}
//    
//	/*
//     'Unknown uniforms' are uniform semantics that have been detected in the PFX file
//     but are unknown to PVRTools. If you wish to utilise this semantic, register
//     the semantic by calling RegisterUniformSemantic(). This is performed above.
//     */
//	if(uiUnknownUniforms)
//	{
//		PVRShellOutputDebug(error.c_str());
//		PVRShellOutputDebug("Unknown uniform semantic count: %u\n", uiUnknownUniforms);
//	}
    
    
    
    
    return true;
}

/*!****************************************************************************
 @Function		LoadVbos
 @Description	Loads the mesh data required for this training course into
				vertex buffer objects
******************************************************************************/
bool RenderBoxMain::LoadVbos(CPVRTString* pErrorStr)
{
	if(!m_Scene.pMesh[0].pInterleaved)
	{
		*pErrorStr = "ERROR: IntroducingPOD requires the pod data to be interleaved. Please re-export with the interleaved option enabled.";
		return false;
	}

	if (!m_puiVbo)      m_puiVbo = new GLuint[m_Scene.nNumMesh];
	if (!m_puiIndexVbo) m_puiIndexVbo = new GLuint[m_Scene.nNumMesh];

	/*
		Load vertex data of all meshes in the scene into VBOs

		The meshes have been exported with the "Interleave Vectors" option,
		so all data is interleaved in the buffer at pMesh->pInterleaved.
		Interleaving data improves the memory access pattern and cache efficiency,
		thus it can be read faster by the hardware.
	*/
	glGenBuffers(m_Scene.nNumMesh, m_puiVbo);
	for (unsigned int i = 0; i < m_Scene.nNumMesh; ++i)
	{
		// Load vertex data into buffer object
		SPODMesh& Mesh = m_Scene.pMesh[i];
		PVRTuint32 uiSize = Mesh.nNumVertex * Mesh.sVertex.nStride;
		glBindBuffer(GL_ARRAY_BUFFER, m_puiVbo[i]);
		glBufferData(GL_ARRAY_BUFFER, uiSize, Mesh.pInterleaved, GL_STATIC_DRAW);

		// Load index data into buffer object if available
		m_puiIndexVbo[i] = 0;
		if (Mesh.sFaces.pData)
		{
			glGenBuffers(1, &m_puiIndexVbo[i]);
			uiSize = PVRTModelPODCountIndices(Mesh) * Mesh.sFaces.nStride;
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_puiIndexVbo[i]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, uiSize, Mesh.sFaces.pData, GL_STATIC_DRAW);
		}
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	return true;
}

/*!****************************************************************************
 @Function		InitApplication
 @Return		bool		true if no error occured
 @Description	Code in InitApplication() will be called by PVRShell once per
				run, before the rendering context is created.
				Used to initialize variables that are not dependant on it
				(e.g. external modules, loading meshes, etc.)
				If the rendering context is lost, InitApplication() will
				not be called again.
******************************************************************************/
bool RenderBoxMain::InitApplication()
{
	m_puiVbo = 0;
	m_puiIndexVbo = 0;
	m_puiTextureIDs = 0;
    m_puiBumpTextureIDs = 0;

    m_BiasMatrix = PVRTMat4(0.5f, 0.0f, 0.0f, 0.0f,
							0.0f, 0.5f, 0.0f, 0.0f,
							0.0f, 0.0f, 0.5f, 0.0f,
							0.5f, 0.5f, 0.5f, 1.0f);
    
	// Get and set the read path for content files
	CPVRTResourceFile::SetReadPath((char*)PVRShellGet(prefReadPath));

	// Get and set the load/release functions for loading external files.
	// In the majority of cases the PVRShell will return NULL function pointers implying that
	// nothing special is required to load external files.
	CPVRTResourceFile::SetLoadReleaseFunctions(PVRShellGet(prefLoadFileFunc), PVRShellGet(prefReleaseFileFunc));

    PVRShellSet(prefOutputInfo, true);
//    PVRShellSet(prefAASamples, 4);
    PVRShellSet(prefOutputFPS, true);
    
	// Load the scene
	if(m_Scene.ReadFromFile(c_szSceneFile) != PVR_SUCCESS)
	{
		PVRShellSet(prefExitMessage, "ERROR: Couldn't load the .pod file\n");
		return false;
	}

	// The cameras are stored in the file. We check it contains at least one.
	if(m_Scene.nNumCamera == 0)
	{
		PVRShellSet(prefExitMessage, "ERROR: The scene does not contain a camera. Please add one and re-export.\n");
		return false;
	}

	// We also check that the scene contains at least one light
	if(m_Scene.nNumLight == 0)
	{
		PVRShellSet(prefExitMessage, "ERROR: The scene does not contain a light. Please add one and re-export.\n");
		return false;
	}
    
	// Initialize variables used for the animation
	m_fFrame = 0;
	m_ulTimePrev = PVRShellGetTime();

	return true;
}

/*!****************************************************************************
 @Function		QuitApplication
 @Return		bool		true if no error occured
 @Description	Code in QuitApplication() will be called by PVRShell once per
				run, just before exiting the program.
				If the rendering context is lost, QuitApplication() will
				not be called.
******************************************************************************/
bool RenderBoxMain::QuitApplication()
{
	// Free the memory allocated for the scene
	m_Scene.Destroy();

	delete[] m_puiVbo;
	delete[] m_puiIndexVbo;

    return true;
}

/*!****************************************************************************
 @Function		InitView
 @Return		bool		true if no error occured
 @Description	Code in InitView() will be called by PVRShell upon
				initialization or after a change in the rendering context.
				Used to initialize variables that are dependant on the rendering
				context (e.g. textures, vertex buffers, etc.)
******************************************************************************/
bool RenderBoxMain::InitView()
{
//    int maxVarying=0;
//    glGetIntegerv(GL_MAX_VARYING_VECTORS, &maxVarying);
    
    PVRShellOutputDebug("GLSL Version = %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    
    m_bOculusSupport = PVRShellGet(prefOculus);
    
    m_sExtensions.LoadExtensions();
    
    
	CPVRTString ErrorStr;

	/*
		Initialize VBO data
	*/
	if(!LoadVbos(&ErrorStr))
	{
		PVRShellSet(prefExitMessage, ErrorStr.c_str());
		return false;
	}

	/*
		Load textures
	*/
	if(!LoadTextures(&ErrorStr))
	{
		PVRShellSet(prefExitMessage, ErrorStr.c_str());
		return false;
	}

	/*
		Load and compile the shaders & link programs
	*/
	if(!LoadPFX(&ErrorStr))
	{
		PVRShellSet(prefExitMessage, ErrorStr.c_str());
		return false;
	}

	/*
		Initialize Print3D
	*/
	bool bRotate = PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen);

	if(m_Print3D.SetTextures(0,PVRShellGet(prefWidth),PVRShellGet(prefHeight), bRotate) != PVR_SUCCESS)
	{
		PVRShellSet(prefExitMessage, "ERROR: Cannot initialise Print3D\n");
		return false;
	}

    m_uiFirstPassFBOWidth = PVRShellGet(prefWidth);
    m_uiFirstPassFBOHeight = PVRShellGet(prefHeight);
    
    m_uiDepthFBOWidth = 2048;
    m_uiDepthFBOHeight = 2048;
    
    m_uiDownScaledFBOWidth = 16;
    m_uiDownScaledFBOHeight = 16;
    
	/*
		Set OpenGL ES render states needed for this training course
	*/
	// Enable backface culling and depth test
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);

	glEnable(GL_DEPTH_TEST);


    //init oculus rift
    if (m_bOculusSupport && InitOVR())
    {
        m_Camera = new OVRCamera();
    }
    else
    {
        m_Camera = new FPSCamera();
    }
    
    if (!CreateFBOS())
    {
        return false;
    }
    
	glClearColor(1.0f, 1.0f, 1.0f, 0.0f);
    

    /*
     Setup camera
     */
    
    m_Camera->Init();
    
    PVRTVec3	vFrom, vTo(0.0f), vUp(0.0f, 1.0f, 0.0f);
    m_Scene.GetCamera( vFrom, vTo, vUp, g_ui32Camera);
    m_Camera->SetPosition(vFrom);
    
	return true;
}

bool RenderBoxMain::CreateFBOS()
{
    //texture target first pass
	glGenTextures(1, &m_uiFirstPassTexture);
    glBindTexture(GL_TEXTURE_2D, m_uiFirstPassTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        
    
    //texture target second pass
	glGenTextures(1, &m_uiSecondPassTexture);
    glBindTexture(GL_TEXTURE_2D, m_uiSecondPassTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    //texture target third pass
	glGenTextures(1, &m_uiThirdPassTexture);
    glBindTexture(GL_TEXTURE_2D, m_uiThirdPassTexture);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    

    //depth texture
	glGenTextures(1, &m_uiDepthTexture);
	glBindTexture(GL_TEXTURE_2D, m_uiDepthTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, m_uiDepthFBOWidth, m_uiDepthFBOHeight, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_INT, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE_EXT);
    
    /*
     Get the currently bound frame buffer object. On most platforms this just gives 0.
	 */
	glGetIntegerv(GL_FRAMEBUFFER_BINDING, &m_i32OriginalFbo);

	bool bMultiSampleIMGSupported = CPVRTgles2Ext::IsGLExtensionSupported("GL_IMG_multisampled_render_to_texture") && m_sExtensions.glFramebufferTexture2DMultisampleIMG != 0;    
    if (bMultiSampleIMGSupported)
    {
        // Query the max amount of samples that are supported, we are going to use the max
        GLint samples;
        glGetIntegerv(GL_MAX_SAMPLES_IMG, &samples);
        
        //first pass
        // create a new depth render buffer
        glGenRenderbuffers(1, &m_uiDepthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, m_uiDepthBuffer);
        m_sExtensions.glRenderbufferStorageMultisampleIMG(GL_RENDERBUFFER, samples, GL_DEPTH_COMPONENT24_OES, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight);
        glBindRenderbuffer(GL_RENDERBUFFER, 0);
        
        glGenFramebuffers(1, &m_uiFirstPassFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiFirstPassFBO);
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_uiDepthBuffer);
        m_sExtensions.glFramebufferTexture2DMultisampleIMG(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_uiFirstPassTexture, 0, samples);

        
        //second pass
        glGenFramebuffers(1, &m_uiSecondPassFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiSecondPassFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_uiSecondPassTexture, 0);
        
        glGenRenderbuffers(1, &m_uiFirstPassDepthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, m_uiFirstPassDepthBuffer);
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24_OES, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight);
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_uiFirstPassDepthBuffer);
        
        //third pass
        glGenFramebuffers(1, &m_uiThirdPassFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiThirdPassFBO);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_uiThirdPassTexture, 0);
        
        //depth FBO
        // Create a frame buffer with only the depth buffer attached
        glGenFramebuffers(1, &m_uiDepthFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiDepthFBO);        
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, m_uiDepthTexture, 0);         
    }
    else
    {
        // create a new depth render buffer
        glGenRenderbuffers(1, &m_uiDepthBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, m_uiDepthBuffer);
        
        glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT24_OES, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight);
        
        glGenFramebuffers(1, &m_uiFirstPassFBO);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiFirstPassFBO);
            
        glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, m_uiDepthBuffer);
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, m_uiFirstPassTexture, 0);
    }
    
    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
    {
        PVRShellSet(prefExitMessage, "ERROR: Frame buffer not set up correctly\n");
        return false;
    }
            
	glBindFramebuffer(GL_FRAMEBUFFER, m_i32OriginalFbo);
    
    return true;
}

/*!****************************************************************************
 @Function		ReleaseView
 @Return		bool		true if no error occured
 @Description	Code in ReleaseView() will be called by PVRShell when the
				application quits or before a change in the rendering context.
******************************************************************************/
bool RenderBoxMain::ReleaseView()
{
	// Deletes the textures
	glDeleteTextures(m_Scene.nNumMaterial, &m_puiTextureIDs[0]);
    glDeleteTextures(m_Scene.nNumMaterial, &m_puiBumpTextureIDs[0]);

	// Frees the texture lookup array
	delete[] m_puiTextureIDs;
    delete[] m_puiBumpTextureIDs;
	m_puiTextureIDs = 0;
    m_puiBumpTextureIDs = 0;

	// Release the effect[s] then the parser
	delete m_pEffectParser;

	// Delete buffer objects
	glDeleteBuffers(m_Scene.nNumMesh, m_puiVbo);
	glDeleteBuffers(m_Scene.nNumMesh, m_puiIndexVbo);

	// Release Print3D Textures
	m_Print3D.ReleaseTextures();

	return true;
}

/*!****************************************************************************
 @Function		RenderScene
 @Return		bool		true if no error occured
 @Description	Main rendering loop function of the program. The shell will
				call this function every frame.
				eglSwapBuffers() will be performed by PVRShell automatically.
				PVRShell will also manage important OS events.
				Will also manage relevent OS events. The user has access to
				these events through an abstraction layer provided by PVRShell.
******************************************************************************/
bool RenderBoxMain::RenderScene()
{
    HandleInput();
    
    if (m_bOculusSupport)
        HandleOVROrientation();
    
    m_Camera->Update();
    
	/*
		Calculates the frame number to animate in a time-based manner.
		Uses the shell function PVRShellGetTime() to get the time in milliseconds.
	*/
	unsigned long ulTime = PVRShellGetTime();

	if(m_ulTimePrev > ulTime)
		m_ulTimePrev = ulTime;

	unsigned long ulDeltaTime = ulTime - m_ulTimePrev;
	m_ulTimePrev	= ulTime;
	m_fFrame += (float)ulDeltaTime * g_fDemoFrameRate;
	if (m_fFrame > m_Scene.nNumFrame - 1) m_fFrame = 0;

	// Sets the scene animation to this frame
	m_Scene.SetFrame(m_fFrame);

	/*
		Set up the view and projection matrices from the camera
	*/
	PVRTMat4 mView, mProjection, mLightView, mLightProjection;
	PVRTVec3	vFrom, vTo(0.0f), vUp(0.0f, 1.0f, 0.0f);
	float fFOV;

	// Setup the camera

	// Camera nodes are after the mesh and light nodes in the array
	int i32CamID = m_Scene.pNode[m_Scene.nNumMeshNode + m_Scene.nNumLight + g_ui32Camera].nIdx;

	// Get the camera position, target and field of view (fov)
	if(m_Scene.pCamera[i32CamID].nIdxTarget != -1) // Does the camera have a target?
		fFOV = m_Scene.GetCameraPos( vFrom, vTo, g_ui32Camera); // vTo is taken from the target node
	else
		fFOV = m_Scene.GetCamera( vFrom, vTo, vUp, g_ui32Camera); // vTo is calculated from the rotation

    std::vector<GLuint> extraTextures;
    
    if (!m_bOculusSupport)
    {
        bool bRotate = PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen);
        mProjection = PVRTMat4::PerspectiveFovRH(fFOV, (float)PVRShellGet(prefWidth)/(float)PVRShellGet(prefHeight), g_fCameraNear, g_fCameraFar, PVRTMat4::OGL, bRotate);

        //render to depth texture from the light point of view
        mLightView = PVRTMat4::LookAtRH(m_Scene.GetLightPosition(0), m_Scene.GetLightPosition(0) + m_Scene.GetLightDirection(0), vUp);
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
        glDepthMask(true);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiDepthFBO);
        glViewport(0, 0, m_uiDepthFBOWidth, m_uiDepthFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        DrawMeshes(mLightView, mProjection, PVRTMat4(),  m_iRenderToDepthEffectId, NULL);
        
        //first pass (render meshes)
        mView = m_Camera->GetTransformMatrix();
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(true);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiFirstPassFBO);
        glViewport(0, 0, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        PVRTMat4 mViewInv = mView.inverse();
        PVRTMat4 mShadowTextureTransform = m_BiasMatrix * mProjection *  mLightView * mViewInv;
        extraTextures.clear();
        extraTextures.push_back(m_uiDepthTexture);
        DrawMeshes(mView, mProjection, mShadowTextureTransform, m_iRenderMeshEffectId, &extraTextures);

        //second pass (render occluder in black)
        
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(true);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiSecondPassFBO);
        glViewport(0, 0, m_uiFirstPassFBOWidth, m_uiFirstPassFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        //render sun
        m_ppPFXEffects[m_iRenderWhiteEffectId]->Activate();
        PVRTMat4 mFrontFacingView = PVRTMat4::Identity();
        //scaling
        mFrontFacingView.f[0] = mView.f[0];
        mFrontFacingView.f[5] = mView.f[5];
        mFrontFacingView.f[10] = mView.f[10];
        
        //translation
        PVRTVec3 camPos = -m_Camera->GetPosition();
        mFrontFacingView.f[12] = mView.f[12];
        mFrontFacingView.f[13] = mView.f[13];
        mFrontFacingView.f[14] = mView.f[14];
        //float fW = PVRShellGet(prefWidth) * 0.5f;
        //float fH = PVRShellGet(prefHeight) * 0.5f;
        //PVRTMat4::Ortho(-fW, fH, fW, -fH, -1.0f, 1.0f, PVRTMat4::OGL, false)
        PVRTMat4 mMVP =  mProjection * mFrontFacingView * PVRTMat4::Translation(camPos);
        glUniformMatrix4fv(m_pUniformMapping[m_iRenderWhiteEffectId][ePVRTPFX_UsWORLDVIEWPROJECTION], 1, GL_FALSE, mMVP.f);
        DrawCircle(m_Scene.GetLightPosition(0), 10.0);

        //render scene
        DrawMeshes(mView, mProjection, PVRTMat4(),  m_iRenderToDepthEffectId, NULL);
        
        //final pass
        glDisable(GL_DEPTH_TEST);
        glDepthMask(false);        
        glBindFramebuffer(GL_FRAMEBUFFER, m_i32OriginalFbo);
        m_ppPFXEffects[m_iLightScatteringEffectId]->Activate();
        glViewport(0, 0, PVRShellGet(prefWidth), PVRShellGet(prefHeight));
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_uiFirstPassTexture);//from geometric pass
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_uiSecondPassTexture);//occluders

        
        PVRTVec4 vLightPosition = m_Scene.GetLightPosition(0);
        PVRTVec4 vLightPositionScreen = mProjection * mView * vLightPosition;
        vLightPositionScreen.x /= vLightPositionScreen.w;
        vLightPositionScreen.y /= vLightPositionScreen.w;
        vLightPositionScreen.x = vLightPositionScreen.x * 0.5 + 0.5;
        vLightPositionScreen.y = vLightPositionScreen.y * 0.5 + 0.5;
        
//        PVRShellOutputDebug("LightPositionScreen %2.2f, %2.2f\n", vLightPositionScreen.x, vLightPositionScreen.y);
        
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_EXPOSURE], 0.25);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_DECAY], 0.975);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_DENSITY], 0.97);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_WEIGHT], 0.5);
        glUniform2f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_LIGHTPOSITIONONSCREEN], vLightPositionScreen.x, vLightPositionScreen.y);
        
        DrawAxisAlignedQuad(PVRTVec2(-1, -1), PVRTVec2(0, 0), PVRTVec2(1, 1), PVRTVec2(1, 1));
        
        //debug
//        glDisable(GL_DEPTH_TEST);
//        glDepthMask(false);
//        glBindFramebuffer(GL_FRAMEBUFFER, m_i32OriginalFbo);
//        m_ppPFXEffects[m_iJustCopyEffectId]->Activate();
//        glViewport(0, 0, PVRShellGet(prefWidth), PVRShellGet(prefHeight));
//        glActiveTexture(GL_TEXTURE0);
//        glBindTexture(GL_TEXTURE_2D, m_uiSecondPassTexture);//from geometric pass
//        DrawAxisAlignedQuad(PVRTVec2(-1, -1), PVRTVec2(0, 0), PVRTVec2(0.1, 0.1), PVRTVec2(1, 1));
        
    }
    else
    {

        //Oculus
        PVRTMat4 mCamera = m_Camera->GetTransformMatrix();
        
        float windowWidth = PVRShellGet(prefWidth);
        float windowHeight = PVRShellGet(prefHeight);
        float scaleFactor = 1.0f / m_DistortionConfig.Scale;

        float w;
        float h;
        float x;
        float y;
        float as;

        bool bRotate = PVRShellGet(prefIsRotated) && PVRShellGet(prefFullScreen);
        mLightProjection = PVRTMat4::PerspectiveFovRH(fFOV, (float)PVRShellGet(prefWidth)/(float)PVRShellGet(prefHeight), g_fCameraNear, g_fCameraFar, PVRTMat4::OGL, bRotate);
        
        //depth from light pov
        mLightView = PVRTMat4::LookAtRH(m_Scene.GetLightPosition(0), m_Scene.GetLightPosition(0) + m_Scene.GetLightDirection(0), vUp);
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);
        glDepthMask(true);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiDepthFBO);
        glViewport(0, 0, m_uiDepthFBOWidth, m_uiDepthFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        DrawMeshes(mLightView, mLightProjection, PVRTMat4(),  m_iRenderToDepthEffectId, NULL);
        
        //render left eye
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(true);        
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiFirstPassFBO);
        glViewport(0, 0, m_uiFirstPassFBOWidth / 2, m_uiFirstPassFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);        
        mView = m_leftEyeAdjust * mCamera;
        mProjection = m_leftEyeProjectionMatrix;        
        PVRTMat4 mViewInv = mView.inverse();
        PVRTMat4 mShadowTextureTransform = m_BiasMatrix * mLightProjection *  mLightView * mViewInv;
        extraTextures.clear();
        extraTextures.push_back(m_uiDepthTexture);
        DrawMeshes(mView, mProjection, mShadowTextureTransform, m_iRenderMeshEffectId, &extraTextures);
        
        
        //light scattering
        //second pass (render occluder in black)
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(true);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiSecondPassFBO);
        glViewport(0, 0, m_uiFirstPassFBOWidth / 2, m_uiFirstPassFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        DrawMeshes(mView, mProjection, PVRTMat4(),  m_iRenderToDepthEffectId, NULL);
        
        //final pass
        glDisable(GL_DEPTH_TEST);
        glDepthMask(false);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiThirdPassFBO);
        m_ppPFXEffects[m_iLightScatteringEffectId]->Activate();
        glViewport(0, 0, m_uiFirstPassFBOWidth / 2, m_uiFirstPassFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);        
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_uiFirstPassTexture);//from geometric pass
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_uiSecondPassTexture);//occluders
                
        PVRTVec4 vLightPosition = m_Scene.GetLightPosition(0);
        PVRTVec4 vLightPositionScreen = mProjection * mView * vLightPosition;
        vLightPositionScreen.x /= vLightPositionScreen.w;
        vLightPositionScreen.y /= vLightPositionScreen.w;
        vLightPositionScreen.x = vLightPositionScreen.x * 0.25 + 0.25;
        vLightPositionScreen.y = vLightPositionScreen.y * 0.5 + 0.5;

        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_EXPOSURE], 0.25);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_DECAY], 0.975);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_DENSITY], 0.97);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_WEIGHT], 0.5);
        glUniform2f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_LIGHTPOSITIONONSCREEN], vLightPositionScreen.x, vLightPositionScreen.y);
        
        DrawAxisAlignedQuad(PVRTVec2(-1, -1), PVRTVec2(0, 0), PVRTVec2(1, 1), PVRTVec2(0.5, 1));
                
        
        //post process oculus lens correction
        glBindFramebuffer(GL_FRAMEBUFFER, m_i32OriginalFbo);
        glViewport(0, 0, windowWidth / 2, windowHeight);

        m_ppPFXEffects[m_iOculusLensEffectId]->Activate();

        w = float(m_leftEyeViewPort.w) / windowWidth;
        h = float(m_leftEyeViewPort.h) / windowHeight;
        x = float(m_leftEyeViewPort.x) / windowWidth;
        y = float(m_leftEyeViewPort.y) / windowHeight;
        as = float(m_leftEyeViewPort.w) / float(m_leftEyeViewPort.h);
        
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsLENSCENTER], x + (w + m_DistortionConfig.XCenterOffset * 0.5f) * 0.5f, y + h * 0.5f);
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsSCREENCENTER], x + w*0.5f, y + h*0.5f);
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsSCALE], (w/2) * scaleFactor, (h/2) * scaleFactor * as);
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsSCALEIN], (2/w), (2/h) / as);
        glUniform4f(m_pUniformMapping[m_iOculusLensEffectId][eUsHMDWARPPARAM], m_DistortionConfig.K[0], m_DistortionConfig.K[1], m_DistortionConfig.K[2], m_DistortionConfig.K[3]);
                
        //draw the FBO texture to a quad into the main on screen FBO
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_uiThirdPassTexture);
        DrawAxisAlignedQuad(PVRTVec2(-1, -1), PVRTVec2(0, 0), PVRTVec2(1, 1), PVRTVec2(0.5, 1));


        ////////////////////////////////////
        
        //render right eye
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiFirstPassFBO);
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(true);
        glViewport(m_uiFirstPassFBOWidth / 2, 0, m_uiFirstPassFBOWidth / 2, m_uiFirstPassFBOHeight);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        mView = m_rightEyeAdjust * mCamera;
        mProjection = m_rightEyeProjectionMatrix;
        mViewInv = mView.inverse();        
        mShadowTextureTransform = m_BiasMatrix * mLightProjection *  mLightView * mViewInv;
        extraTextures.clear();
        extraTextures.push_back(m_uiDepthTexture);
        DrawMeshes(mView, mProjection, mShadowTextureTransform, m_iRenderMeshEffectId, &extraTextures);
        
        
        //light scattering
        //second pass (render occluder in black)
        glEnable(GL_DEPTH_TEST);
        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
        glDepthMask(true);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiSecondPassFBO);
        glViewport(m_uiFirstPassFBOWidth / 2, 0, m_uiFirstPassFBOWidth / 2, m_uiFirstPassFBOHeight);
        DrawMeshes(mView, mProjection, PVRTMat4(),  m_iRenderToDepthEffectId, NULL);
        
        //final pass
        glDisable(GL_DEPTH_TEST);
        glDepthMask(false);
        glBindFramebuffer(GL_FRAMEBUFFER, m_uiThirdPassFBO);
        m_ppPFXEffects[m_iLightScatteringEffectId]->Activate();
        glViewport(m_uiFirstPassFBOWidth / 2, 0, m_uiFirstPassFBOWidth / 2, m_uiFirstPassFBOHeight);
        //glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_uiFirstPassTexture);//from geometric pass
        glActiveTexture(GL_TEXTURE1);
        glBindTexture(GL_TEXTURE_2D, m_uiSecondPassTexture);//occluders
        
        vLightPosition = m_Scene.GetLightPosition(0);
        vLightPositionScreen = mProjection * mView * vLightPosition;
        vLightPositionScreen.x /= vLightPositionScreen.w;
        vLightPositionScreen.y /= vLightPositionScreen.w;
        vLightPositionScreen.x = vLightPositionScreen.x * 0.25 + 0.75;
        vLightPositionScreen.y = vLightPositionScreen.y * 0.5 + 0.5;
        
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_EXPOSURE], 0.25);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_DECAY], 0.975);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_DENSITY], 0.97);
        glUniform1f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_WEIGHT], 0.5);
        glUniform2f(m_pUniformMapping[m_iLightScatteringEffectId][eUsLS_LIGHTPOSITIONONSCREEN], vLightPositionScreen.x, vLightPositionScreen.y);
        
        DrawAxisAlignedQuad(PVRTVec2(-1, -1), PVRTVec2(0.5, 0), PVRTVec2(1, 1), PVRTVec2(1.0, 1));
        
        //post process oculus lens correction
        glBindFramebuffer(GL_FRAMEBUFFER, m_i32OriginalFbo);
        glViewport(windowWidth / 2, 0, windowWidth / 2, windowHeight);
        
        m_ppPFXEffects[m_iOculusLensEffectId]->Activate();
        w = float(m_rightEyeViewPort.w) / float(windowWidth);
        h = float(m_rightEyeViewPort.h) / float(windowHeight);
        x = float(m_rightEyeViewPort.x) / float(windowWidth);
        y = float(m_rightEyeViewPort.y) / float(windowHeight);
        as = float(m_rightEyeViewPort.w) / float(m_rightEyeViewPort.h);
        
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsLENSCENTER], x + (w - m_DistortionConfig.XCenterOffset * 0.5f) * 0.5f, y + h * 0.5f);
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsSCREENCENTER], x + w*0.5f, y + h*0.5f);
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsSCALE], (w/2.0) * scaleFactor, (h/2.0) * scaleFactor * as);
        glUniform2f(m_pUniformMapping[m_iOculusLensEffectId][eUsSCALEIN], (2.0/w), (2.0/h) / as);
        glUniform4f(m_pUniformMapping[m_iOculusLensEffectId][eUsHMDWARPPARAM], m_DistortionConfig.K[0], m_DistortionConfig.K[1], m_DistortionConfig.K[2], m_DistortionConfig.K[3]);
        
        
        //draw the FBO texture to a quad into the main on screen FBO
        //don't clear since the left eye have already been rendered
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, m_uiThirdPassTexture);
        DrawAxisAlignedQuad(PVRTVec2(-1, -1), PVRTVec2(0.5, 0), PVRTVec2(1, 1), PVRTVec2(1, 1));

        
        
        
        
        
        //debug
//        glDisable(GL_DEPTH_TEST);
//        glDepthMask(false);
//        glBindFramebuffer(GL_FRAMEBUFFER, m_i32OriginalFbo);
//        m_ppPFXEffects[m_iAmbientOcclusionEffectId]->Activate();
//        glViewport(0, 0, PVRShellGet(prefWidth), PVRShellGet(prefHeight));
//        glActiveTexture(GL_TEXTURE0);
//        glBindTexture(GL_TEXTURE_2D, m_uiDepthTexture);//from geometric pass
//        DrawAxisAlignedQuad(PVRTVec2(-1, -1), PVRTVec2(0, 0), PVRTVec2(0.1, 0.1), PVRTVec2(1, 1));
        
    }
    
	return true;
}

void RenderBoxMain::DrawMeshes(const PVRTMat4 &mView, const PVRTMat4 &mProjection, const PVRTMat4 &mShadowTextureTransform, int effectId, const std::vector<GLuint> *extraTextures)
{
    
	/*
     A scene is composed of nodes. There are 3 types of nodes:
     - MeshNodes :
     references a mesh in the pMesh[].
     These nodes are at the beginning of the pNode[] array.
     And there are nNumMeshNode number of them.
     This way the .pod format can instantiate several times the same mesh
     with different attributes.
     - lights
     - cameras
     To draw a scene, you must go through all the MeshNodes and draw the referenced meshes.
     */
    
    m_ppPFXEffects[effectId]->Activate();
    
	/*
     Get the direction of the first light from the scene.
     */
	PVRTVec4 vLightDirection;
	vLightDirection = m_Scene.GetLightDirection(0);
    
    // Retrieve the list of required uniforms
	const CPVRTArray<SPVRTPFXUniform>& aUniforms = m_ppPFXEffects[effectId]->GetUniformArray();
    

	for (unsigned int i = 0; i < m_Scene.nNumMeshNode; ++i)
	{
		SPODNode& Node = m_Scene.pNode[i];
        // Gets pMesh referenced by the pNode
        int i32MeshIndex = m_Scene.pNode[i].nIdx;
		SPODMesh* pMesh = &m_Scene.pMesh[i32MeshIndex];
        
        // bind the VBO for the mesh
        glBindBuffer(GL_ARRAY_BUFFER, m_puiVbo[i32MeshIndex]);
        // bind the index buffer, won't hurt if the handle is 0
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_puiIndexVbo[i32MeshIndex]);
        
		// Get the node model matrix
		PVRTMat4 mWorld;
		mWorld = m_Scene.GetWorldMatrix(Node);
        
		// Pass the model-view-projection matrix (MVP) to the shader to transform the vertices
		PVRTMat4 mModelView, mMVP;
		mModelView = mView * mWorld;
		mMVP = mProjection * mModelView;
        
		// Pass the light direction in model space to the shader
		PVRTVec4 vLightDirModel = mWorld.inverse() * vLightDirection;
                
		/*
         Now we loop over the uniforms requested by the PFX file.
         Using the switch statement allows us to handle all of the required semantics
         */
		for(unsigned int j = 0; j < aUniforms.GetSize(); ++j)
		{
			switch(aUniforms[j].nSemantic)
			{
                case ePVRTPFX_UsPOSITION:
				{
					glVertexAttribPointer(aUniforms[j].nLocation, 3, GL_FLOAT, GL_FALSE, pMesh->sVertex.nStride, pMesh->sVertex.pData);
					glEnableVertexAttribArray(aUniforms[j].nLocation);
				}
                    break;
                case ePVRTPFX_UsNORMAL:
				{
					glVertexAttribPointer(aUniforms[j].nLocation, 3, GL_FLOAT, GL_FALSE, pMesh->sNormals.nStride, pMesh->sNormals.pData);
					glEnableVertexAttribArray(aUniforms[j].nLocation);
				}
                    break;
                case ePVRTPFX_UsTANGENT: 
				{
					glVertexAttribPointer(aUniforms[j].nLocation, 3, GL_FLOAT, GL_FALSE, pMesh->sTangents.nStride, pMesh->sTangents.pData);
					glEnableVertexAttribArray(aUniforms[j].nLocation);
				}
                    break;                    
                case ePVRTPFX_UsUV:
				{
					glVertexAttribPointer(aUniforms[j].nLocation, 2, GL_FLOAT, GL_FALSE, pMesh->psUVW[0].nStride, pMesh->psUVW[0].pData);
					glEnableVertexAttribArray(aUniforms[j].nLocation);
				}
                    break;
                case ePVRTPFX_UsWORLDVIEWPROJECTION:
				{
					glUniformMatrix4fv(aUniforms[j].nLocation, 1, GL_FALSE, mMVP.f);
				}
                    break;
                    
                case ePVRTPFX_UsWORLDVIEW:
				{
					glUniformMatrix4fv(aUniforms[j].nLocation, 1, GL_FALSE, mModelView.f);
				}
                    break;
                    
                case ePVRTPFX_UsPROJECTION:
				{
					glUniformMatrix4fv(aUniforms[j].nLocation, 1, GL_FALSE, mProjection.f);
				}
                    break;
                    
                    
                case ePVRTPFX_UsEYEPOSMODEL:
				{
                    PVRTVec3 eyePos = PVRTMat3(mWorld.inverse()) * m_Camera->GetPosition();
					glUniform3f(aUniforms[j].nLocation, eyePos.x, eyePos.y, eyePos.z);
				}
                    break;
                    
                case ePVRTPFX_UsWORLDVIEWIT:
				{
					PVRTMat4 mWorldViewI, mWorldViewIT;
                    
					// Passes the inverse transpose of the world-view matrix to the shader to transform the normals
					mWorldViewI  = mModelView.inverse();
					mWorldViewIT = mWorldViewI.transpose();
                    
					PVRTMat3 WorldViewIT = PVRTMat3(mWorldViewIT);
                    
					glUniformMatrix3fv(aUniforms[j].nLocation, 1, GL_FALSE, WorldViewIT.f);
				}
                    break;
                case ePVRTPFX_UsLIGHTDIRMODEL:
				{
					glUniform3f(aUniforms[j].nLocation, vLightDirModel.x, vLightDirModel.y, vLightDirModel.z);
				}
                    break;
                    
                case ePVRTPFX_UsTEXTURE:
				{
					// Set the sampler variable to the texture unit
					glUniform1i(aUniforms[j].nLocation, aUniforms[j].nIdx);
				}
                    break;
                    
                case eUsSHADOWTEXTURETRANSFORM:
				{
					// Set the sampler variable to the texture unit
					glUniformMatrix4fv(aUniforms[j].nLocation, 1, GL_FALSE, mShadowTextureTransform.f);
				}
                    break;
                    
                case eUsTEXELSTEP:
				{
					// Set the sampler variable to the texture unit
					glUniform2f(aUniforms[j].nLocation, 1.0 / m_uiDepthFBOWidth, 1.0 / m_uiDepthFBOHeight);
				}
                    break;
                    
                    
			}
		}
        
		// Load the correct texture using our texture lookup table
		GLuint uiTex = 0;
        GLuint uiTexBump = 0;
        
		if(Node.nIdxMaterial != -1)
        {
			uiTex = m_puiTextureIDs[Node.nIdxMaterial];
            uiTexBump = m_puiBumpTextureIDs[Node.nIdxMaterial];
        }
        
        glActiveTexture(GL_TEXTURE0);
        glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, uiTex);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        
        
        glActiveTexture(GL_TEXTURE1);
        glEnable(GL_TEXTURE_2D);        
        glBindTexture(GL_TEXTURE_2D, uiTexBump);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        
        
        //bind all passed extra textures
        if (extraTextures != NULL)
        {
            int textureUnit = GL_TEXTURE1 + 1;
            for (int i=0; i < extraTextures->size(); ++i)
            {
                glActiveTexture(textureUnit + i);
                glEnable(GL_TEXTURE_2D);
                glBindTexture(GL_TEXTURE_2D, (*extraTextures)[i]);
//                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
//                glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
                
            }
        }
        
		/*
         Now that the model-view matrix is set and the materials are ready,
         call another function to actually draw the mesh.
         */
		DrawMesh(Node.nIdx);
	}
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    
    /*
     Now disable all of the enabled attribute arrays that the PFX requested.
     */
    for(unsigned int j = 0; j < aUniforms.GetSize(); ++j)
    {
        switch(aUniforms[j].nSemantic)
        {
			case ePVRTPFX_UsPOSITION:
			case ePVRTPFX_UsNORMAL:
			case ePVRTPFX_UsUV:
            {
                glDisableVertexAttribArray(aUniforms[j].nLocation);
            }
				break;
        }
    }
    
}

/*!****************************************************************************
 @Function		DrawMesh
 @Input			i32NodeIndex		Node index of the mesh to draw
 @Description	Draws a SPODMesh after the model view matrix has been set and
				the meterial prepared.
******************************************************************************/
void RenderBoxMain::DrawMesh(int i32NodeIndex)
{
	int i32MeshIndex = m_Scene.pNode[i32NodeIndex].nIdx;
	SPODMesh* pMesh = &m_Scene.pMesh[i32MeshIndex];
	/*
		The geometry can be exported in 4 ways:
		- Indexed Triangle list
		- Non-Indexed Triangle list
		- Indexed Triangle strips
		- Non-Indexed Triangle strips
	*/

	if(pMesh->nNumStrips == 0)
	{
		if(m_puiIndexVbo[i32MeshIndex])
		{
			// Indexed Triangle list

			// Are our face indices unsigned shorts? If they aren't, then they are unsigned ints
			GLenum type = (pMesh->sFaces.eType == EPODDataUnsignedShort) ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT;
			glDrawElements(GL_TRIANGLES, pMesh->nNumFaces*3, type, 0);
		}
		else
		{
			// Non-Indexed Triangle list
			glDrawArrays(GL_TRIANGLES, 0, pMesh->nNumFaces*3);
		}
	}
	else
	{
		PVRTuint32 offset = 0;

		// Are our face indices unsigned shorts? If they aren't, then they are unsigned ints
		GLenum type = (pMesh->sFaces.eType == EPODDataUnsignedShort) ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT;

		for(int i = 0; i < (int)pMesh->nNumStrips; ++i)
		{
			if(m_puiIndexVbo[i32MeshIndex])
			{
				// Indexed Triangle strips
				glDrawElements(GL_TRIANGLE_STRIP, pMesh->pnStripLength[i]+2, type, (void*) (offset * pMesh->sFaces.nStride));
			}
			else
			{
				// Non-Indexed Triangle strips
				glDrawArrays(GL_TRIANGLE_STRIP, offset, pMesh->pnStripLength[i]+2);
			}
			offset += pMesh->pnStripLength[i]+2;
		}
	}
}

void RenderBoxMain::HandleInput()
{
    m_Camera->FeedWithKeyEvent(PVRShellIsKeyPressed(PVRShellKeyNameUP), PVRShellIsKeyPressed(PVRShellKeyNameDOWN),
                               PVRShellIsKeyPressed(PVRShellKeyNameLEFT), PVRShellIsKeyPressed(PVRShellKeyNameRIGHT));
    
    float *vec2PointerDeltaLocation = (float*)PVRShellGet(prefPointerDeltaLocation);
    m_Camera->FeedWithMouseMoveEvent(vec2PointerDeltaLocation[0], vec2PointerDeltaLocation[1]);
}

void RenderBoxMain::HandleOVROrientation()
{
    Quatf hmdOrient = m_OVRSensorFusion.GetOrientation();
    Matrix4f hmdMat(hmdOrient);
    
    PVRTMat4 ovrOrientation;
    ConvertOVRToPVRTMatrix(hmdMat, ovrOrientation);
    
    m_Camera->SetOrientation(ovrOrientation);    
}

/*!***************************************************************************
 @Function		PVRTPFXOnLoadTexture
 @Input			TextureName
 @Output			uiHandle
 @Output			uiFlags
 @Return			EPVRTError	PVR_SUCCESS on success.
 @Description	Callback for texture load.
 *****************************************************************************/
EPVRTError RenderBoxMain::PVRTPFXOnLoadTexture(const CPVRTStringHash& TextureName, GLuint& uiHandle, unsigned int& uiFlags)
{
	/*
     This is an optional callback function for PVRTPFXEffect and can be used to automate
     the texture loading process.
     If multiple effects are to be loaded and they share textures it would be
     prudent to have a caching system in place so texture memory is not wasted.
     Please see OGLES2MagicLantern for an example of this.
     */
//	if(PVRTTextureLoadFromPVR(TextureName.String().c_str(), &uiHandle) != PVR_SUCCESS)
//		return PVR_FAIL;
    
	return PVR_SUCCESS;
}

/*!****************************************************************************
 @Function		DrawFullScreenQuad
 @Input			afLowerLeft		Lower left corner of the quad in normalized device coordinates
 afUpperRight    Upper right corner of the quad in normalized device coordinates
 @Description	Draws a textured fullscreen quad
 ******************************************************************************/
void RenderBoxMain::DrawAxisAlignedQuad(PVRTVec2 afLowerLeft, PVRTVec2 afUpperRight)
{
	glDisable(GL_DEPTH_TEST);
    
	// Enable vertex arributes
	glEnableVertexAttribArray(AXIS_ALIGNED_QUAD_VERTEX_ARRAY);
	glEnableVertexAttribArray(AXIS_ALIGNED_QUAD_TEXCOORD_ARRAY);
    
	const float afVertexData[] = { afLowerLeft.x, afLowerLeft.y,  afUpperRight.x, afLowerLeft.y,
        afLowerLeft.x, afUpperRight.y,  afUpperRight.x, afUpperRight.y };
	glVertexAttribPointer(VERTEX_ARRAY, 2, GL_FLOAT, GL_FALSE, 0, afVertexData);
    
	const float afTexCoordData[] = { 0, 0,  1, 0,  0, 1,  1, 1 };
	glVertexAttribPointer(NORMAL_ARRAY, 2, GL_FLOAT, GL_FALSE, 0, afTexCoordData);
    
	// Draw the quad
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
	// Disable vertex arributes
	glDisableVertexAttribArray(AXIS_ALIGNED_QUAD_VERTEX_ARRAY);
	glDisableVertexAttribArray(AXIS_ALIGNED_QUAD_TEXCOORD_ARRAY);
    
	// Disable blending
	glEnable(GL_DEPTH_TEST);
}

/*!****************************************************************************
 @Function		DrawFullScreenQuad
 @Input			afLowerLeft		Lower left corner of the quad in normalized device coordinates
 afLowerLeftUV   UV coordinates of lower left corner
 afUpperRight    Upper right corner of the quad in normalized device coordinates
 afUpperRightUV	UV coordinates of upper right corner
 @Description	Draws a textured fullscreen quad
 ******************************************************************************/
void RenderBoxMain::DrawAxisAlignedQuad(PVRTVec2 afLowerLeft, PVRTVec2 afLowerLeftUV,
									  PVRTVec2 afUpperRight, PVRTVec2 afUpperRightUV)
{
	glDisable(GL_DEPTH_TEST);
    
	// Enable vertex arributes
	glEnableVertexAttribArray(AXIS_ALIGNED_QUAD_VERTEX_ARRAY);
	glEnableVertexAttribArray(AXIS_ALIGNED_QUAD_TEXCOORD_ARRAY);
    
	const float afVertexData[] = { afLowerLeft.x, afLowerLeft.y,  afUpperRight.x, afLowerLeft.y,
        afLowerLeft.x, afUpperRight.y,  afUpperRight.x, afUpperRight.y };
	glVertexAttribPointer(VERTEX_ARRAY, 2, GL_FLOAT, GL_FALSE, 0, afVertexData);
    
	//const float afTexCoordData[] = { afLowerLeftUV.x, afLowerLeftUV.y,  afUpperRightUV.x, afLowerLeftUV.y,
	//								 afLowerLeftUV.x, afUpperRightUV.y,  afUpperRightUV.x, afUpperRightUV.y };
	float afTexCoordData[8];
	afTexCoordData[0] = afLowerLeftUV.x;
	afTexCoordData[1] = afLowerLeftUV.y;
	afTexCoordData[2] = afUpperRightUV.x;
	afTexCoordData[3] = afLowerLeftUV.y;
	afTexCoordData[4] = afLowerLeftUV.x;
	afTexCoordData[5] = afUpperRightUV.y;
	afTexCoordData[6] = afUpperRightUV.x;
	afTexCoordData[7] = afUpperRightUV.y;
    
	glVertexAttribPointer(NORMAL_ARRAY, 2, GL_FLOAT, GL_FALSE, 0, afTexCoordData);
    
    
	// Draw the quad
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
	// Disable vertex arributes
	glDisableVertexAttribArray(AXIS_ALIGNED_QUAD_VERTEX_ARRAY);
	glDisableVertexAttribArray(AXIS_ALIGNED_QUAD_TEXCOORD_ARRAY);
    
	glEnable(GL_DEPTH_TEST);
}


/*!****************************************************************************
 @Function		DrawFullScreenQuad
 @Input			afLowerLeft		Lower left corner of the quad in normalized device coordinates
 afLowerLeftUV   UV coordinates of lower left corner
 afUpperRight    Upper right corner of the quad in normalized device coordinates
 afUpperRightUV	UV coordinates of upper right corner
 @Description	Draws a textured fullscreen quad
 ******************************************************************************/
void RenderBoxMain::DrawPoint(PVRTVec4 position)
{    
	// Enable vertex arributes
	glEnableVertexAttribArray(0);
    
	const float afVertexData[] = { position.x, position.y, position.z };
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, afVertexData);
    
	// Draw the point
	glDrawArrays(GL_POINTS, 0, 1);
    
	// Disable vertex arributes
	glDisableVertexAttribArray(0);
}

void RenderBoxMain::DrawCircle(PVRTVec4 position, float radius)
{
    const int kNbVertex = 20;
    struct Vertex
    {
        float x,y,z;
    };
    
    Vertex afVertexData[kNbVertex + 1];
    float coef = 2.0 * 3.14159 / kNbVertex;
    for (int i=0; i<kNbVertex; i++)
    {
        float angle = i * coef;
        afVertexData[i].x = position.x + cos(angle) * radius;
        afVertexData[i].y = position.y + sin(angle) * radius;
        afVertexData[i].z = position.z;
    }

    afVertexData[kNbVertex].x = position.x;
    afVertexData[kNbVertex].y = position.y;
    afVertexData[kNbVertex].z = position.z;
    
    glEnableVertexAttribArray(0);    
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, afVertexData);
	glDrawArrays(GL_TRIANGLE_FAN, 0, kNbVertex + 1);
	glDisableVertexAttribArray(0);
}

bool RenderBoxMain::InitOVR()
{
    PVRShellOutputDebug("---==== Initializing Oculus Rift... ====---\n");
    
    System::Init(Log::ConfigureDefaultLog(LogMask_All));
    
    Ptr<DeviceManager> pManager;
    pManager = *DeviceManager::Create();
    m_pOVRHMD = *pManager->EnumerateDevices<HMDDevice>().CreateDevice();
    
    if (m_pOVRHMD == NULL)
    {
        PVRShellOutputDebug("Oculus Rift NOT Found. \n");
        return false;
    }

    if (m_pOVRHMD->GetDeviceInfo(&m_OVRhmdInfo))
    {
        PVRShellOutputDebug("Oculus Rift Found %s\n", m_OVRhmdInfo.DisplayDeviceName);
        

        PVRShellOutputDebug("Getting Sensor\n");
        
        m_pOVRSensor = *m_pOVRHMD->GetSensor();
        
        if (!m_pOVRSensor)
        {
            PVRShellOutputDebug("Fail to get Sensor\n");
            return false;
        }
        
        PVRShellOutputDebug("Attaching SensorFusion\n");
        m_OVRSensorFusion.AttachToSensor(m_pOVRSensor);
        
        PVRShellOutputDebug("Fetching Stereo Config\n");
        StereoConfig ovrStereoConfig;
        
        ovrStereoConfig.SetFullViewport(Viewport(0,0, PVRShellGet(prefWidth), PVRShellGet(prefHeight)));
        ovrStereoConfig.SetStereoMode(Stereo_LeftRight_Multipass);
        ovrStereoConfig.SetHMDInfo(m_OVRhmdInfo);
        ovrStereoConfig.Set2DAreaFov(DegreeToRad(85.0f));
        
        // Configure proper Distortion Fit.
        // For 7" screen, fit to touch left side of the view, leaving a bit of
        // invisible screen on the top (saves on rendering cost).
        // For smaller screens (5.5"), fit to the top.
        if (m_OVRhmdInfo.HScreenSize > 0.0f)
        {
            if (m_OVRhmdInfo.HScreenSize > 0.140f)  // 7"
                ovrStereoConfig.SetDistortionFitPointVP(-1.0f, 0.0f);
            else
                ovrStereoConfig.SetDistortionFitPointVP(0.0f, 1.0f);
        }
        
        StereoEyeParams ovrLeftEyeParams  = ovrStereoConfig.GetEyeRenderParams(StereoEye_Left);
        StereoEyeParams ovrRightEyeParams = ovrStereoConfig.GetEyeRenderParams(StereoEye_Right);
        
        ConvertOVRToPVRTMatrix(ovrLeftEyeParams.Projection, m_leftEyeProjectionMatrix);
        ConvertOVRToPVRTMatrix(ovrRightEyeParams.Projection, m_rightEyeProjectionMatrix);
        m_leftEyeProjectionMatrix = m_leftEyeProjectionMatrix.transpose();
        m_rightEyeProjectionMatrix = m_rightEyeProjectionMatrix.transpose();
        
        ConvertOVRToPVRTMatrix(ovrLeftEyeParams.ViewAdjust, m_leftEyeAdjust);
        ConvertOVRToPVRTMatrix(ovrRightEyeParams.ViewAdjust, m_rightEyeAdjust);
        m_leftEyeAdjust = m_leftEyeAdjust.transpose();
        m_rightEyeAdjust = m_rightEyeAdjust.transpose();
        
        m_DistortionConfig = ovrStereoConfig.GetDistortionConfig();

        m_OVRRenderScale = ovrStereoConfig.GetDistortionScale();
        m_uiFirstPassFBOWidth = PVRShellGet(prefWidth) * m_OVRRenderScale;
        m_uiFirstPassFBOHeight = PVRShellGet(prefHeight) * m_OVRRenderScale;
        
        m_leftEyeViewPort = ovrLeftEyeParams.VP;
        PVRShellOutputDebug("Left Eye Viewport: %d %d %d %d\n", m_leftEyeViewPort.x, m_leftEyeViewPort.y, m_leftEyeViewPort.w, m_leftEyeViewPort.h);
        
        m_rightEyeViewPort = ovrRightEyeParams.VP;
        PVRShellOutputDebug("Right Eye Viewport: %d %d %d %d\n", m_rightEyeViewPort.x, m_rightEyeViewPort.y, m_rightEyeViewPort.w, m_rightEyeViewPort.h);
        
    }

    return true;
}


/*!****************************************************************************
 @Function		NewDemo
 @Return		PVRShell*		The demo supplied by the user
 @Description	This function must be implemented by the user of the shell.
				The user should return its PVRShell object defining the
				behaviour of the application.
******************************************************************************/
PVRShell* NewDemo()
{
	return new RenderBoxMain();
}

/******************************************************************************
 End of file (RenderBoxMain.cpp)
******************************************************************************/

