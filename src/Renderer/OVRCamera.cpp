//
//  OVRCamera.cpp
//  MiniGE
//
//  Created by Mathieu Guindon on 2012-08-29.
//  Copyright (c) 2012 Mathieu Guindon. All rights reserved.
//

#include "OVRCamera.h"
#include "PVRTQuaternion.h"
#include "PVRShell.h"

static float DegToRad(float degree)
{
    return degree * M_PI / 180.0f;
}

OVRCamera::OVRCamera() :
    ICamera(),
    mPosition(0,3,35),
    mOrientation(0,0,0),
    mSpeed(0.2),
    mMouseSensitivity(12),
    mMouseDeltaX(0),
    mMouseDeltaY(0),
    mMovingForward(false),
    mMovingBackward(false),
    mStrafingLeft(false),
    mStrafingRight(false)
{
    
}

OVRCamera::~OVRCamera()
{
    
}

void OVRCamera::SetPosition(const PVRTVec3 &pos)
{
    mPosition = pos;
}

void OVRCamera::SetSpeed(float speed)
{
    mSpeed = speed;
}

void OVRCamera::SetOrientation(const PVRTMat4 &orientation)
{
    mOVRTrackerOrientation = orientation;
}

void OVRCamera::UpdatePosition()
{
    // Break up our movement into components along the X, Y and Z axis
    float camMovementXComponent = 0.0f;
    float camMovementYComponent = 0.0f;
    float camMovementZComponent = 0.0f;
    
    if (mMovingBackward)
    {
        // Control X-Axis movement        
        float pitchFactor = float(cos(DegToRad(mOrientation.x)));
        camMovementXComponent += ( mSpeed * float(sin(DegToRad(mOrientation.y))) ) * pitchFactor;
        
        // Control Y-Axis movement
        camMovementYComponent += mSpeed * float(sin(DegToRad(mOrientation.x))) * -1.0f;
        
        // Control Z-Axis movement
        float yawFactor = float(cos(DegToRad(mOrientation.x)));
        camMovementZComponent += ( mSpeed * float(cos(DegToRad(mOrientation.y))) * -1.0f ) * yawFactor;
    }
    
    if (mMovingForward)
    {
        // Control X-Axis movement
        float pitchFactor = cos(DegToRad(mOrientation.x));
        camMovementXComponent += ( mSpeed * float(sin(DegToRad(mOrientation.y))) * -1.0f) * pitchFactor;
        
        // Control Y-Axis movement
        camMovementYComponent += mSpeed * float(sin(DegToRad(mOrientation.x)));
        
        // Control Z-Axis movement
        float yawFactor = float(cos(DegToRad(mOrientation.x)));
        camMovementZComponent += ( mSpeed * float(cos(DegToRad(mOrientation.y))) ) * yawFactor;
    }
    
    if (mStrafingLeft)
    {
        // Calculate our Y-Axis rotation in radians once here because we use it twice
        float yRotRad = DegToRad(mOrientation.y);
        
        camMovementXComponent += -mSpeed * float(cos(yRotRad));
        camMovementZComponent += -mSpeed * float(sin(yRotRad));
    }
    
    if (mStrafingRight)
    {
        // Calculate our Y-Axis rotation in radians once here because we use it twice
        float yRotRad = DegToRad(mOrientation.y);
        
        camMovementXComponent += mSpeed * float(cos(yRotRad));
        camMovementZComponent += mSpeed * float(sin(yRotRad));
    }
    
    // After combining our movements for any & all keys pressed, assign them to our OVRCamera speed along the given axis
    PVRTVec3 camSpeed;
    camSpeed.x = -camMovementXComponent;
    camSpeed.y = 0;//camMovementYComponent; uncomment to fly
    camSpeed.z = camMovementZComponent;
    
    // Cap the speeds to our movementSpeedFactor (otherwise going forward and strafing at an angle is twice as fast as just going forward!)
    // X Speed cap
    if (camSpeed.x > mSpeed)
    {
        //cout << "high capping X speed to: " << movementSpeedFactor << endl;
        camSpeed.x = mSpeed;
    }
    if (camSpeed.x < -mSpeed)
    {
        //cout << "low capping X speed to: " << movementSpeedFactor << endl;
        camSpeed.x = -mSpeed;
    }
    
    // Y Speed cap
    if (camSpeed.y  > mSpeed)
    {
        //cout << "low capping Y speed to: " << movementSpeedFactor << endl;
        camSpeed.y  = mSpeed;
    }
    if (camSpeed.y  < -mSpeed)
    {
        //cout << "high capping Y speed to: " << movementSpeedFactor << endl;
        camSpeed.y  = -mSpeed;
    }
    
    // Z Speed cap
    if (camSpeed.z > mSpeed)
    {
        //cout << "high capping Z speed to: " << movementSpeedFactor << endl;
        camSpeed.z = mSpeed;
    }
    if (camSpeed.z < -mSpeed)
    {
        //cout << "low capping Z speed to: " << movementSpeedFactor << endl;
        camSpeed.z = -mSpeed;
    }
    
    mPosition -= camSpeed;
   
//    printf("%2.2f, %2.2f, %2.2f", mPosition.x, mPosition.y, mPosition.z);

//    std::cout <<"OVRCamera velocity vector: " << camSpeed.x << ", " << camSpeed.y << ", " << camSpeed.z << std::endl;
//    std::cout <<"OVRCamera position vector: " << mPosition.x << ", " << mPosition.y << ", " << mPosition.z << std::endl;

}

void OVRCamera::UpdateOrientation()
{
    mOrientation.x += mMouseDeltaY / mMouseSensitivity;
    mOrientation.y += mMouseDeltaX / mMouseSensitivity;
    
    // Control looking up and down with the mouse forward/back movement
    // Limit loking up to vertically up
    if (mOrientation.x < -90.0f)
    {
        mOrientation.x = -90.0f;
    }
    
    // Limit looking down to vertically down
    if (mOrientation.x > 90.0f)
    {
        mOrientation.x = 90.0f;
    }
    
    // Looking left and right. Keep the angles in the range -180.0f (anticlockwise turn looking behind) to 180.0f (clockwise turn looking behind)
    if (mOrientation.y  < -180.0f)
    {
        mOrientation.y  += 360.0f;
    }
    
    if (mOrientation.y  > 180.0f)
    {
        mOrientation.y  -= 360.0f;
    }
}

void OVRCamera::FeedWithKeyEvent(bool forward, bool backward, bool left, bool right)
{    
    mMovingForward = forward;
    mMovingBackward = backward;
    mStrafingLeft = left;
    mStrafingRight = right;
}

void OVRCamera::FeedWithMouseMoveEvent(float deltaX, float deltaY)
{
    mMouseDeltaX = -deltaX;
    mMouseDeltaY = -deltaY;
    
}

void OVRCamera::Update()
{
    UpdateOrientation();
    UpdatePosition();
    mMouseDeltaX = 0;
    mMouseDeltaY = 0;
}

PVRTMat4 OVRCamera::GetTransformMatrix() const
{
//    Quaternion rotationX = Quaternion::CreateFromAxisAngle(PVRTVec3(1,0,0), DegToRad(mOrientation.x));
//    glm::quat rotationX = glm::angleAxis(DegToRad(mOrientation.x), PVRTVec3(1,0,0));
    PVRTQUATERNIONf rotationX;
    PVRTMatrixQuaternionRotationAxisF(rotationX, PVRTVec3(1,0,0), DegToRad(mOrientation.x));
    
//    Quaternion rotationY = Quaternion::CreateFromAxisAngle(PVRTVec3(0,1,0), DegToRad(mOrientation.y));
//    glm::quat rotationY = glm::angleAxis(DegToRad(mOrientation.y), PVRTVec3(0,1,0));
    PVRTQUATERNIONf rotationY;
    PVRTMatrixQuaternionRotationAxisF(rotationY, PVRTVec3(0,1,0), DegToRad(mOrientation.y));

//    return PVRTMat4_cast(rotationX) * PVRTMat4_cast(rotationY) * glm::translate(PVRTMat4(1), PVRTVec3(-mPosition.x, -mPosition.y, -mPosition.z));
    PVRTMat4 rotMatX, rotMatY;
    
    PVRTMatrixRotationQuaternionF(rotMatX, rotationX);
    PVRTMatrixRotationQuaternionF(rotMatY, rotationY);
    
    return mOVRTrackerOrientation * PVRTMat4::Translation(-mPosition);
}


PVRTMat4 OVRCamera::GetProjectionMatrix(int screenWidth, int screenHeight) const
{

    //double h = 4.0 * screenHeight / screenWidth;
    
    //return glm::frustum<float>(-2.0, 2.0, -h / 2.0, h / 2.0, 2.0, 700.0);
    return PVRTMat4();
    //mProjectionMatrix = PVRTMat4::Ortho(-10, 10, -10, 10, -10, 10);
    
}