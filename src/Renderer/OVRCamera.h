//
//  Camera.h
//  MiniGE
//
//  Created by Mathieu Guindon on 2012-08-29.
//  Copyright (c) 2012 Mathieu Guindon. All rights reserved.
//

#ifndef __MiniGE__OVRCamera__
#define __MiniGE__OVRCamera__

#include "ICamera.h"

class OVRCamera : public ICamera
{
public:
    OVRCamera();
    virtual ~OVRCamera();

    virtual void Init() {}
    virtual void SetPosition(const PVRTVec3 &pos);
    virtual void SetSpeed(float speed);
    virtual void SetMouseSensitivity(float sensitivity) { mMouseSensitivity = sensitivity;}
    virtual void SetOrientation(const PVRTMat4 &orientation);

    virtual void FeedWithKeyEvent(bool forward, bool backward, bool left, bool right);
    virtual void FeedWithMouseMoveEvent(float deltaX, float deltaY);

    virtual void Update();

    virtual PVRTMat4 GetTransformMatrix() const;
    virtual PVRTVec3 GetPosition() const { return mPosition; }
    virtual PVRTVec3 GetDirection() const { return mOrientation; }
    virtual PVRTMat4 GetProjectionMatrix(int screenWidth, int screenHeight) const;

protected:
    void UpdatePosition();
    void UpdateOrientation();

    PVRTVec3 mPosition;
    PVRTVec3 mOrientation;
    PVRTMat4 mOVRTrackerOrientation;
    
    float mSpeed;
    float mMouseSensitivity;

    float mMouseDeltaX;
    float mMouseDeltaY;
    bool mMovingForward;
    bool mMovingBackward;
    bool mStrafingLeft;
    bool mStrafingRight;

};

#endif /* defined(__MiniGE__Camera__) */
