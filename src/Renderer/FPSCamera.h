//
//  Camera.h
//  MiniGE
//
//  Created by Mathieu Guindon on 2012-08-29.
//  Copyright (c) 2012 Mathieu Guindon. All rights reserved.
//

#ifndef __MiniGE__FPSCamera__
#define __MiniGE__FPSCamera__

#include "ICamera.h"

class FPSCamera : public ICamera
{
public:
    FPSCamera();
    virtual ~FPSCamera();

    virtual void Init() {}
    virtual void SetPosition(const PVRTVec3 &pos);
    virtual void SetSpeed(float speed);
    virtual void SetMouseSensitivity(float sensitivity) { mMouseSensitivity = sensitivity;}
    virtual void SetOrientation(const PVRTMat4 &orientation) { }
    
    virtual void FeedWithKeyEvent(bool forward, bool backward, bool left, bool right);
    virtual void FeedWithMouseMoveEvent(float deltaX, float deltaY);

    virtual void Update();

    virtual PVRTMat4 GetTransformMatrix() const;
    virtual PVRTVec3 GetPosition() const { return mPosition; }
    virtual PVRTVec3 GetDirection() const { return mOrientation; }
    virtual PVRTMat4 GetProjectionMatrix(int screenWidth, int screenHeight) const;

protected:
    void UpdatePosition();
    void UpdateOrientation();

    PVRTVec3 mPosition;
    PVRTVec3 mOrientation;
    float mSpeed;
    float mMouseSensitivity;

    float mMouseDeltaX;
    float mMouseDeltaY;
    bool mMovingForward;
    bool mMovingBackward;
    bool mStrafingLeft;
    bool mStrafingRight;

};

#endif /* defined(__MiniGE__Camera__) */
