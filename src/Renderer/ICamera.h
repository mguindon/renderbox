//
//  ICamera.h
//  MiniGE
//
//  Created by Mathieu Guindon on 2012-12-02.
//  Copyright (c) 2012 Mathieu Guindon. All rights reserved.
//

#ifndef MiniGE_ICamera_h
#define MiniGE_ICamera_h

#include "PVRTVector.h"


class ICamera
{
public:
    ICamera() {}
    virtual ~ICamera() {}
    
    virtual void Init() = 0;
    virtual void SetPosition(const PVRTVec3 &pos) = 0;
    virtual void SetSpeed(float speed) = 0;
    virtual void SetMouseSensitivity(float sensitivity) = 0;
    virtual void SetOrientation(const PVRTMat4 &orientation) = 0;
    
    virtual void FeedWithKeyEvent(bool forward, bool backward, bool left, bool right) = 0;
    virtual void FeedWithMouseMoveEvent(float deltaX, float deltaY) = 0;
    virtual void Update() = 0;
    
    virtual PVRTMat4 GetTransformMatrix() const = 0;
    virtual PVRTVec3 GetPosition() const = 0;
    virtual PVRTVec3 GetDirection() const = 0;
    virtual PVRTMat4 GetProjectionMatrix(int screenWidth, int screenHeight) const = 0;
};

#endif
