/******************************************************************************

 @File         AppController.h

 @Title        

 @Version      

 @Copyright    Copyright (c) Imagination Technologies Limited.

 @Platform     

 @Description  

******************************************************************************/
#ifndef _APPCONTROLLER_H_
#define _APPCONTROLLER_H_

#include "PVRShell.h"

//CLASS INTERFACES:

@interface AppController : NSObject <NSApplicationDelegate>
{
	NSTimer*				mainLoopTimer;	// timer for the main loop
	PVRShellInit*           pvrshellInit;
}

- (void) terminateApp;

@end

@interface AppWindow : NSWindow < NSWindowDelegate>
{
    PVRShellInit* pvrshell;
    unsigned int mouseButtonState;
}

@property (assign) PVRShellInit * pvrshell;
@property (assign) unsigned int mouseButtonState;
@end

#endif //_APPCONTROLLER_H_

