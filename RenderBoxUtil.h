#ifndef __RenderBox__Util__
#define __RenderBox__Util__

namespace OVR
{
    class Matrix4f;
}

class PVRTMat4;

void ConvertOVRToPVRTMatrix(const OVR::Matrix4f &ovrMatrix, PVRTMat4 &pvrtMatrix);

#endif /* defined(__RenderBox__Util__) */
