
#include "RenderBoxUtil.h"
#include "PVRTVector.h"
#include "OVR.h"


void ConvertOVRToPVRTMatrix(const OVR::Matrix4f &ovrMatrix, PVRTMat4 &pvrtMatrix)
{
    pvrtMatrix.f[0] = ovrMatrix.M[0][0];
    pvrtMatrix.f[1] = ovrMatrix.M[0][1];
    pvrtMatrix.f[2] = ovrMatrix.M[0][2];
    pvrtMatrix.f[3] = ovrMatrix.M[0][3];
    pvrtMatrix.f[4] = ovrMatrix.M[1][0];
    pvrtMatrix.f[5] = ovrMatrix.M[1][1];
    pvrtMatrix.f[6] = ovrMatrix.M[1][2];
    pvrtMatrix.f[7] = ovrMatrix.M[1][3];
    pvrtMatrix.f[8] = ovrMatrix.M[2][0];
    pvrtMatrix.f[9] = ovrMatrix.M[2][1];
    pvrtMatrix.f[10] = ovrMatrix.M[2][2];
    pvrtMatrix.f[11] = ovrMatrix.M[2][3];
    pvrtMatrix.f[12] = ovrMatrix.M[3][0];
    pvrtMatrix.f[13] = ovrMatrix.M[3][1];
    pvrtMatrix.f[14] = ovrMatrix.M[3][2];
    pvrtMatrix.f[15] = ovrMatrix.M[3][3];
}